package bookr.dl.repository.impl;

import java.util.ArrayList;
import java.util.List;

import bookr.bl.entity.Books;
import scbpress.publisher.model.PublishedBook;

public abstract class PublishedBookUtils {

	public static List<Books> publishedBookToBooks(List<PublishedBook> publishedBooks) {
		List<Books> result = new ArrayList<>();
		for (PublishedBook publishedBook : publishedBooks) {
			Books book = new Books();
			populate(publishedBook, book);
			result.add(book);
		}		
		return result;
	}

	private static void populate(PublishedBook publishedBook, Books book) {
		book.setId(publishedBook.getId());
		book.setName(publishedBook.getBookName());
		book.setAuthor(publishedBook.getAuthorName());
		book.setPrice(publishedBook.getPrice());
	}	
	
}
