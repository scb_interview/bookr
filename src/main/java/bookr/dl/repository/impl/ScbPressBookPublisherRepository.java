package bookr.dl.repository.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import bookr.bl.entity.Books;
import bookr.dl.repository.api.IBookPublisherRepository;
import scbpress.publisher.model.PublishedBook;

@Repository
public class ScbPressBookPublisherRepository implements IBookPublisherRepository {

	@Autowired
	private RestTemplate restTemplate;
	
	@Value("${bookr.scbpress.publishingApi.findAllBooksEndPointUrl}")
	private String findAllBooksEndPointUrl;
	
	@Override
	public List<Books> findAllBooks() {
		List<Books> result = this.getPublishedBookList(this.findAllBooksEndPointUrl);
		return result;
	}

	@Value("${bookr.scbpress.publishingApi.findRecommendedBooksUrl}")
	private String findRecommendedBooksUrl;	
	
	@Override
	public List<Books> findRecommendedBooks() {
		List<Books> result = this.getPublishedBookList(this.findRecommendedBooksUrl);
		return result;
	}

	private List<Books> getPublishedBookList(String url) {
		ResponseEntity<List<PublishedBook>> response = this.restTemplate.exchange(
				  url,
				  HttpMethod.GET,
				  null,
				  new ParameterizedTypeReference<List<PublishedBook>>(){});
		List<PublishedBook> elements = response.getBody();
		List<Books> result = PublishedBookUtils.publishedBookToBooks(elements);
		return result;			
	}
	
}
