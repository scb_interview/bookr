package bookr.dl.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import bookr.bl.entity.Books;

public interface IBookRepsitory extends JpaRepository<Books, Integer> {

	@Modifying
	@Query("UPDATE Books SET recommended = false")
	int resetBooksRecommended();
	
	@Query("SELECT b FROM Books b ORDER BY CASE WHEN(b.recommended = true) THEN 1 WHEN(b.recommended = false) THEN 0 END DESC, b.name ASC")
	List<Books> findAllOrderByRecommendedDescNameAsc();
	
}
