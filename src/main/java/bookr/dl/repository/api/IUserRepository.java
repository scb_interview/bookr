package bookr.dl.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import bookr.bl.entity.Users;

public interface IUserRepository extends JpaRepository<Users, Long> {

	Users findByAccountUsername(String accountUsername);

}
