package bookr.dl.repository.api;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import bookr.bl.entity.Orders;

public interface IOrderRepository extends JpaRepository<Orders, Long> {

	List<Orders> findByUserAccountUsername(String username);
	
}
