package bookr.dl.repository.api;

import java.util.List;

import bookr.bl.entity.Books;

public interface IBookPublisherRepository {

	List<Books> findAllBooks();
	
	List<Books> findRecommendedBooks();
	
}
