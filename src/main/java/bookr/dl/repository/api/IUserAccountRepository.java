package bookr.dl.repository.api;

import org.springframework.data.jpa.repository.JpaRepository;

import bookr.bl.entity.UserAccounts;

public interface IUserAccountRepository extends JpaRepository<UserAccounts, String>{

}
