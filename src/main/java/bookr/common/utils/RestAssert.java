package bookr.common.utils;

import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;

public abstract class RestAssert {

	public static void hasLength(@Nullable String text, String message) {
		if (!StringUtils.hasLength(text)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
		}
	}	
	
	public static void isTrue(boolean expression, String message) {
		if (!expression) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, message);
		}
	}
	
}
