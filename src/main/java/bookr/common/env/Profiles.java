package bookr.common.env;

public class Profiles {

	public static final String ENVIRONMENT_DEV = "env_dev";
	public static final String ENVIRONMENT_PROD = "env_prod";

	public static final String FEATURE_DBCONSOLE = "ft_dbconsole";
	public static final String FEATURE_SWAGGERUI = "ft_swaggerui";
	
	
}
