package bookr.bl.service.api;

import java.util.List;

import bookr.bl.entity.Books;

public interface IBookService {

	List<Books> findAllBook();
	
	void synchBooksFromPublisher();
	
}
