package bookr.bl.service.api.excp;

public class AlreadyLoggedInException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public AlreadyLoggedInException() {
	}
	
	public AlreadyLoggedInException(String message) {
		super(message);
	}
	
}
