package bookr.bl.service.api.excp;

public class UsernameAlreadyExistException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public UsernameAlreadyExistException() {
	}

	public UsernameAlreadyExistException(String message) {
		super(message);
	}
	
}
