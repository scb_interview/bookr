package bookr.bl.service.api.excp;

public class InvalidOrderException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public InvalidOrderException() {
	}

	public InvalidOrderException(String message) {
		super(message);
	}

}
