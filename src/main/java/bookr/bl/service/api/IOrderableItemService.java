package bookr.bl.service.api;

import bookr.bl.entity.IOrderableItem;

public interface IOrderableItemService {

	IOrderableItem findOrderableItemByKindAndId(String kind, Long id);
	
}
