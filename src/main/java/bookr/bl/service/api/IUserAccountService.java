package bookr.bl.service.api;

import bookr.bl.entity.UserAccounts;
import bookr.bl.service.api.param.CreateUserAccountRequest;

public interface IUserAccountService {

	void createUserAndUserAccount(CreateUserAccountRequest createUserAccountRequest);
	
	void createUserAccount(UserAccounts userAccounts);
	
	UserAccounts loadUserAccount(String username);
	
}
