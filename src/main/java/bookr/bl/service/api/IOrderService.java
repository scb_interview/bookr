package bookr.bl.service.api;

import java.util.List;

import bookr.bl.entity.IOrderableItem;
import bookr.bl.entity.Orders;
import bookr.bl.service.api.param.CreateOrderRequest;
import bookr.bl.service.api.param.CreateOrderResponse;

public interface IOrderService {

	CreateOrderResponse createOrder(CreateOrderRequest createOrderRequest);
	
	List<Orders> findOrderByAccountUsername(String username);
	
	List<IOrderableItem> findDistinctOrderedProductByKind(String username, String kind);
	
}
