package bookr.bl.service.api;

import bookr.bl.entity.UserAccounts;
import bookr.bl.service.api.param.LoginRequest;

public interface IUserSessionService {

	void login(LoginRequest loginRequest);
	
	UserAccounts getLoggedInUserAccount();
	
	void logout();
	
}
