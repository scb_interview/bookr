package bookr.bl.service.api;

import bookr.bl.entity.Users;

public interface IUserService {

	void createUser(Users user);
	
	Users findUserByUsername(String username);
	
}
