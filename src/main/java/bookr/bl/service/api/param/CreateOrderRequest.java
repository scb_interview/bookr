package bookr.bl.service.api.param;

import java.util.ArrayList;
import java.util.List;

import bookr.bl.entity.Users;

public class CreateOrderRequest {

	private Users user;
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}

	private List<CreateOrderRequestOrderItem> items = new ArrayList<>();
	public List<CreateOrderRequestOrderItem> getItems() {
		return items;
	}
	public void setItems(List<CreateOrderRequestOrderItem> items) {
		this.items = items;
	}
	
}
