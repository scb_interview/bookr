package bookr.bl.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bookr.bl.entity.Books;
import bookr.bl.entity.OrderItem;
import bookr.bl.entity.IOrderableItem;
import bookr.bl.service.api.IOrderableItemService;

@Service
@Transactional
public class OrderableItemService implements IOrderableItemService {

	@Autowired
	private EntityManager entityManager;
	
	private Map<String,Class<? extends IOrderableItem>> mapProductKindToEntityClassName = new HashMap<>();
	{
		this.mapProductKindToEntityClassName.put(OrderItem.PRODUCTKIND_BOOK, Books.class);
	}
	
	@Override
	public IOrderableItem findOrderableItemByKindAndId(String kind, Long id) {
		Class<? extends IOrderableItem> entityClass = this.mapProductKindToEntityClassName.get(kind);
		IOrderableItem orderableItem = this.entityManager.find(entityClass, id);
		return orderableItem;
	}

}
