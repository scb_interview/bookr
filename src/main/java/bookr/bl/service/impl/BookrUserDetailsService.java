package bookr.bl.service.impl;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import bookr.bl.entity.UserAccounts;
import bookr.bl.service.api.IUserAccountService;

@Transactional
public class BookrUserDetailsService implements UserDetailsService {

	private IUserAccountService userAccountService;	
	public IUserAccountService getUserAccountService() {
		return userAccountService;
	}
	public void setUserAccountService(IUserAccountService userAccountService) {
		this.userAccountService = userAccountService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserAccounts userAccounts = this.userAccountService.loadUserAccount(username);
		if (userAccounts == null) {
			throw new UsernameNotFoundException("username : " + username + " Not found");
		}
		return userAccounts;
	}

}
