package bookr.bl.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bookr.bl.entity.Books;
import bookr.bl.service.api.IBookService;
import bookr.dl.repository.api.IBookPublisherRepository;
import bookr.dl.repository.api.IBookRepsitory;

@Service
@Transactional
public class BookService implements IBookService {

	@Autowired
	private IBookRepsitory bookRepository;
	
	@Override
	public List<Books> findAllBook() {
		List<Books> result = this.bookRepository.findAllOrderByRecommendedDescNameAsc();
		return result;
	}

	@Autowired
	private IBookPublisherRepository bookPublisherRepository;
	
	int count;
	
	@Override
	public void synchBooksFromPublisher() {
		List<Books> books = this.bookPublisherRepository.findAllBooks();
		List<Books> recommendedBooks = this.bookPublisherRepository.findRecommendedBooks();
		
		// clear recommended flag
		this.bookRepository.resetBooksRecommended();
		
		// merge
		for (Books book : books) {
			this.bookRepository.save(book);
		}
		
		// update recommended flag
		for (Books recommendedBook : recommendedBooks) {
			recommendedBook.setRecommended(true);
			this.bookRepository.save(recommendedBook);
		}
		
	}

}
