package bookr.bl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bookr.bl.entity.UserAccounts;
import bookr.bl.service.api.IUserSessionService;
import bookr.bl.service.api.excp.AlreadyLoggedInException;
import bookr.bl.service.api.param.LoginRequest;

@Service
@Transactional
public class UserSessionService implements IUserSessionService {

	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Override
	public void login(LoginRequest loginRequest) {
		UserAccounts loggedInUserAccount = this.getLoggedInUserAccount();
		if (loggedInUserAccount != null) {
			throw new AlreadyLoggedInException("Already logged in as : " + loggedInUserAccount.getUsername());
		}
		
		String username = loginRequest.getUsername();
		String password = loginRequest.getPassword();
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username,password);
		Authentication authentication = authenticationManager.authenticate(authToken);
		
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(authentication);
	}

	@Override
	public UserAccounts getLoggedInUserAccount() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();

		Object principal = authentication.getPrincipal();
		
		// anonymous principal type is string
		if (!(principal instanceof UserAccounts)) {
			return null;
		}
		
		UserAccounts userAccount = (UserAccounts)principal; 
		
		return userAccount;
	}

	@Override
	public void logout() {
		SecurityContextHolder.clearContext();
	}

}
