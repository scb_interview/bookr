package bookr.bl.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import bookr.bl.entity.UserAccounts;
import bookr.bl.entity.Users;
import bookr.bl.service.api.IUserAccountService;
import bookr.bl.service.api.IUserService;
import bookr.bl.service.api.excp.UsernameAlreadyExistException;
import bookr.bl.service.api.param.CreateUserAccountRequest;
import bookr.dl.repository.api.IUserAccountRepository;

@Service
@Transactional
public class UserAccountService implements IUserAccountService {

	@Autowired
	private IUserAccountRepository userAccountRepository;
	
	@Override
	public void createUserAccount(UserAccounts userAccounts) {
		this.userAccountRepository.save(userAccounts);
	}

	@Override
	public UserAccounts loadUserAccount(String username) {
		Optional<UserAccounts> optional = this.userAccountRepository.findById(username);
		UserAccounts result = null;
		if (optional.isPresent()) {
			result = optional.get();
		}
		return result;
	}	
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private IUserService userService;
		
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void createUserAndUserAccount(CreateUserAccountRequest createUserAccountRequest) {
		String username = createUserAccountRequest.getUsername();
		String password = createUserAccountRequest.getPassword();
		
		{
			// check if username duplicated
			boolean usernameExist = this.userAccountRepository.existsById(username);
			if (usernameExist) {
				throw new UsernameAlreadyExistException("Username : " + username + " already exists");
			}			
		}
		
		UserAccounts userAccount = new UserAccounts();
		userAccount.setUsername(username);
		String encordedPassword = this.passwordEncoder.encode(password);
		userAccount.setPassword(encordedPassword);
		this.createUserAccount(userAccount);
		
		Users user = new Users();
		user.setAccountUsername(username);
		user.setFirstname(createUserAccountRequest.getFirstname());
		user.setLastName(createUserAccountRequest.getLastName());
		user.setDateOfBirth(createUserAccountRequest.getDateOfBirth());
		this.userService.createUser(user);
	}

	
}
