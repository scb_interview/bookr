package bookr.bl.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import bookr.bl.entity.IOrderableItem;
import bookr.bl.entity.OrderItem;
import bookr.bl.entity.OrderableItem;
import bookr.bl.entity.Orders;
import bookr.bl.service.api.IOrderService;
import bookr.bl.service.api.IOrderableItemService;
import bookr.bl.service.api.excp.InvalidOrderException;
import bookr.bl.service.api.param.CreateOrderRequest;
import bookr.bl.service.api.param.CreateOrderRequestOrderItem;
import bookr.bl.service.api.param.CreateOrderResponse;
import bookr.dl.repository.api.IOrderRepository;

@Service
@Transactional
public class OrderService implements IOrderService {

	@Autowired
	private IOrderRepository orderRepository;
	
	@Autowired
	private IOrderableItemService orderableItemService;
	
	@Override
	public CreateOrderResponse createOrder(CreateOrderRequest createOrderRequest) {
		CreateOrderResponse result = new CreateOrderResponse();
		Orders order = new Orders();
		order.setUser(createOrderRequest.getUser());
		order.setCreatedDate(new Date());
		
		for (CreateOrderRequestOrderItem requestItem: createOrderRequest.getItems()) {
			OrderItem orderItem = new OrderItem();
			orderItem.setProductKind(requestItem.getProductKind());
			orderItem.setProductId(requestItem.getProductId());
			orderItem.setQty(requestItem.getQty());

			// resolve price/name
			// need to keep them as documents not reference
			IOrderableItem orderableItem = this.orderableItemService.findOrderableItemByKindAndId(orderItem.getProductKind(), orderItem.getProductId());
			
			if (orderableItem == null) {
				throw new InvalidOrderException("Invalid product id : " + requestItem.getProductId());
			}
			
			orderItem.setName(orderableItem.getName());
			orderItem.setPrice(orderableItem.getPrice());
			
			order.getOrderItems().add(orderItem);
		}
		
		order.calculateTotalAmount();
		
		Orders savedOrder = this.orderRepository.save(order);

		result.setTotalAmount(order.getTotalAmount());
		result.setOrderId(savedOrder.getId());
		
		return result;
	}
	
	@Override
	public List<Orders> findOrderByAccountUsername(String username) {
		List<Orders> result = this.orderRepository.findByUserAccountUsername(username);
		return result;
	}

	@Override
	public List<IOrderableItem> findDistinctOrderedProductByKind(String username, String kind) {
		List<Orders> orders = this.findOrderByAccountUsername(username);
		Map<Long, IOrderableItem> idToOrderableItem = new HashMap<>();
		for (Orders order : orders) {
			for (OrderItem item : order.getOrderItems()) {
				if (!kind.equals(item.getProductKind())) {
					continue;
				}
				OrderableItem orderableItem = new OrderableItem();
				orderableItem.setId(item.getProductId());
				orderableItem.setKind(item.getProductKind());
				orderableItem.setName(item.getName());
				orderableItem.setPrice(item.getPrice());
				idToOrderableItem.put(item.getProductId(), orderableItem);
			}
		}
		
		return new ArrayList<>(idToOrderableItem.values());
	}

}
