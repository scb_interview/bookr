package bookr.bl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import bookr.bl.entity.Users;
import bookr.bl.service.api.IUserService;
import bookr.dl.repository.api.IUserRepository;

@Service
@Transactional
public class UserService implements IUserService {

	@Autowired
	private IUserRepository userRepository;
	
	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public void createUser(Users user) {			
		this.userRepository.save(user);
	}

	@Override
	public Users findUserByUsername(String username) {
		Users user = this.userRepository.findByAccountUsername(username);
		return user;
	}

}
