package bookr.bl.job;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import bookr.bl.service.api.IBookService;

@Component
public class ScheduledTasks {

	private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
	
	@Autowired
	private IBookService bookService;
	
	@Scheduled(cron = "${bookr.job.updateBookFromPublisher.cron}")
	public void updateBookFromPublisher() {
		logger.info("Start synch books from publisher at " + new Date());
		try {
			this.bookService.synchBooksFromPublisher();
			logger.info("Synch book done at " + new Date());
		} catch (Exception e) {
			logger.warn("Synch book fail at " + new Date());
		}
	}
	
}
