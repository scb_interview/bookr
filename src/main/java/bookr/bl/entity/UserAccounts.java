package bookr.bl.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
public class UserAccounts implements UserDetails {
	private static final long serialVersionUID = 1L;
	
	@Transient
	private List<GrantedAuthority> grantedAuthoritys = new ArrayList<>();
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.grantedAuthoritys;
	}
	public void setGrantedAuthoritys(List<GrantedAuthority> grantedAuthoritys) {
		this.grantedAuthoritys = grantedAuthoritys;
	}

	private String password;
	@Override
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Id
	private String username;
	@Override
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	private boolean accountNonExpired = true;
	@Override
	public boolean isAccountNonExpired() {
		return this.accountNonExpired;
	}
	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	private boolean accountNonLocked = true;
	@Override
	public boolean isAccountNonLocked() {
		return this.accountNonLocked;
	}
	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}
	
	private boolean credentialsNonExpired = true;
	@Override
	public boolean isCredentialsNonExpired() {
		return this.credentialsNonExpired;
	}
	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	private boolean enabled = true;
	@Override
	public boolean isEnabled() {
		return this.enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

}
