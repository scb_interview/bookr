package bookr.bl.entity;

import java.math.BigDecimal;

public interface IOrderableItem {

	Long getId();
	
	String getKind();
	
	String getName();
	
	BigDecimal getPrice();
	
}
