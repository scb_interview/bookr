package bookr.bl.entity;

import java.math.BigDecimal;

public class OrderableItem implements IOrderableItem {

	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	private String kind;
	public void setKind(String kind) {
		this.kind = kind;
	}
	@Override
	public String getKind() {
		return this.kind;
	}

	private String name;
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String getName() {
		return this.name;
	}

	private BigDecimal price;
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	@Override
	public BigDecimal getPrice() {
		return this.price;
	}

}
