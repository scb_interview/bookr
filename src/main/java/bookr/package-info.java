/**
 * 
 */
/**
 * Represents the root level package of the bookr web service.
 * 
 * @author cherdsak.r
 *
 */
package bookr;