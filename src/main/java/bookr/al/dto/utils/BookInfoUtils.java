package bookr.al.dto.utils;

import java.util.ArrayList;
import java.util.List;

import bookr.al.dto.BookInfoDto;
import bookr.bl.entity.Books;

public abstract class BookInfoUtils {

	public static void populate(BookInfoDto bookInfo, Books book) {
		bookInfo.setId(book.getId());
		bookInfo.setName(book.getName());
		bookInfo.setAuthor(book.getAuthor());
		bookInfo.setPrice(book.getPrice());
		bookInfo.setReccommended(book.isRecommended());
	}
	
	public static List<BookInfoDto> convert(List<Books> books) {
		List<BookInfoDto> result = new ArrayList<>();
		for (Books book : books) {	
			BookInfoDto bookInfo = new BookInfoDto();
			populate(bookInfo, book);
			result.add(bookInfo);
		}
		return result;
	}
	
}
