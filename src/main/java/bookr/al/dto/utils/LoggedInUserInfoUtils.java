package bookr.al.dto.utils;

import bookr.al.dto.LoggedInUserInfoDto;
import bookr.bl.entity.Users;

public abstract class LoggedInUserInfoUtils {

	public static void populate(LoggedInUserInfoDto loggedInUserInfo, Users users) {
		loggedInUserInfo.setName(users.getFirstname());
		loggedInUserInfo.setSurname(users.getLastName());
		loggedInUserInfo.setDateOfBirth(users.getDateOfBirth());
	}
	
}
