package bookr.al.dto;

import java.util.ArrayList;
import java.util.List;

public class CreateBookOrderRequestDto {

	private List<Long> orders = new ArrayList<>();
	public List<Long> getOrders() {
		return orders;
	}
	public void setOrders(List<Long> orders) {
		this.orders = orders;
	}
	
}
