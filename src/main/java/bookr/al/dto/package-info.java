/**
 * 
 */
/**
 * Defines Data Transfer Object for the controller layer of bookr web service.
 * @author cherdsak.r
 *
 */
package bookr.al.dto;