package bookr.al.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import bookr.common.utils.MoneySerializer;

/**
 * 
 * 
 * @author cherdsak.r
 *
 */
public class BookInfoDto {

	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private String author;
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	@JsonSerialize(using=MoneySerializer.class)
	private BigDecimal price;
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@JsonProperty("is_reccommended")
	private boolean reccommended;
	public boolean isReccommended() {
		return reccommended;
	}
	public void setReccommended(boolean reccommended) {
		this.reccommended = reccommended;
	}
	
}
