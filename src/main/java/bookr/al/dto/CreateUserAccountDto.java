package bookr.al.dto;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;

public class CreateUserAccountDto {

	@ApiModelProperty(required = true, position=0, example = "first.last")
	private String username;	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	@ApiModelProperty(required = true, position=1, example = "abc123")	
	private String password;	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@ApiModelProperty(required = false, position=2, example = "20/12/1998")
	private Date dateOfBirth;
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	
}
