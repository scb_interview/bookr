package bookr.al.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import bookr.common.utils.MoneySerializer;

public class CreateBookOrderResponseDto {

	@JsonSerialize(using=MoneySerializer.class)
	private BigDecimal price;
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
}
