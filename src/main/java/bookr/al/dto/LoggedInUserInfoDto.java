package bookr.al.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * represent 
 * 
 * @author cherdsak.r
 *
 */
public class LoggedInUserInfoDto {

	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	private String surname;
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}

	private Date dateOfBirth;
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	private List<Long> books = new ArrayList<>();
	public List<Long> getBooks() {
		return books;
	}
	public void setBooks(List<Long> books) {
		this.books = books;
	}
	
}
