package bookr.al.dto;

/**
 * Represent client's request for login to bookr web service. 
 * 
 * @author cherdsak.r
 *
 */
public class LoginRequestDto {

	private String username;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	private String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
