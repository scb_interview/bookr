/**
 * 
 */
/**
 * Defines controllers for serving REST api for bookr web service.
 * 
 * @author cherdsak.r
 *
 */
package bookr.al.controller;