package bookr.al.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import bookr.al.dto.CreateUserAccountDto;
import bookr.bl.service.api.IUserAccountService;
import bookr.bl.service.api.excp.UsernameAlreadyExistException;
import bookr.bl.service.api.param.CreateUserAccountRequest;
import bookr.common.utils.RestAssert;

@RestController
public class UserAccountController {
	
	@Autowired
	private IUserAccountService userAccountService;
	
	@PostMapping("users")
	public void createUserAccount(@RequestBody CreateUserAccountDto createUserAccountDto) {
		CreateUserAccountRequest createUserAccountRequest = new CreateUserAccountRequest();
		createUserAccountRequest.setUsername(createUserAccountDto.getUsername());
		createUserAccountRequest.setPassword(createUserAccountDto.getPassword());
		
		String[] name = createUserAccountDto.getUsername().split("\\.");
		RestAssert.isTrue(name.length == 2, "username must be in following format : " + "<name>.<surname>");
		
		String firstName = name[0]; 
		String lastName = name[1];
		
		createUserAccountRequest.setFirstname(firstName);
		createUserAccountRequest.setLastName(lastName);
		
		createUserAccountRequest.setDateOfBirth(createUserAccountDto.getDateOfBirth());
		
		try {
			this.userAccountService.createUserAndUserAccount(createUserAccountRequest);			
		} catch (UsernameAlreadyExistException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
		}
		
	}
	
	
	
}
