package bookr.al.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import bookr.al.dto.CreateBookOrderRequestDto;
import bookr.al.dto.CreateBookOrderResponseDto;
import bookr.bl.entity.OrderItem;
import bookr.bl.entity.UserAccounts;
import bookr.bl.entity.Users;
import bookr.bl.service.api.IOrderService;
import bookr.bl.service.api.IUserService;
import bookr.bl.service.api.IUserSessionService;
import bookr.bl.service.api.excp.InvalidOrderException;
import bookr.bl.service.api.param.CreateOrderRequest;
import bookr.bl.service.api.param.CreateOrderRequestOrderItem;
import bookr.bl.service.api.param.CreateOrderResponse;

@RestController
public class BookOrderController {

	@Autowired
	private IOrderService orderService;
	
	@Autowired
	private IUserSessionService userSessionService;
	
	@Autowired
	private IUserService userService;
	
	@PostMapping("/users/orders")
	public CreateBookOrderResponseDto createOrder(@RequestBody CreateBookOrderRequestDto createOrderRequestDto) {
		
		UserAccounts userAccount = this.userSessionService.getLoggedInUserAccount();	
		CreateOrderRequest createOrderRequest = new CreateOrderRequest();
		
		Users user = userService.findUserByUsername(userAccount.getUsername());				
		
		createOrderRequest.setUser(user);
		
		for (Long bookId : createOrderRequestDto.getOrders()) {
			CreateOrderRequestOrderItem createOrderRequestOrderItem = new CreateOrderRequestOrderItem();
			createOrderRequestOrderItem.setProductKind(OrderItem.PRODUCTKIND_BOOK);
			createOrderRequestOrderItem.setProductId(bookId);
			
			createOrderRequest.getItems().add(createOrderRequestOrderItem);
		}
		
		CreateOrderResponse createOrderResponse = null;
		try {
			createOrderResponse = this.orderService.createOrder(createOrderRequest);			
		} catch (InvalidOrderException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}

		CreateBookOrderResponseDto createBookOrderResponseDto = new CreateBookOrderResponseDto();
		createBookOrderResponseDto.setPrice(createOrderResponse.getTotalAmount());
		
		return createBookOrderResponseDto;
	}
	
}
