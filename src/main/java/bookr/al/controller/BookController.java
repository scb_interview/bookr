package bookr.al.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import bookr.al.dto.BookInfoDto;
import bookr.al.dto.utils.BookInfoUtils;
import bookr.bl.entity.Books;
import bookr.bl.service.api.IBookService;

@RestController
public class BookController {

	@Autowired
	private IBookService bookService;
	
	/**
	 * 
	 * @return
	 */
	@GetMapping("books")
	public List<BookInfoDto> getBookInfos() {
		List<Books> books = this.bookService.findAllBook();
		List<BookInfoDto> result = BookInfoUtils.convert(books);
		return result;
	}
	
}
