package bookr.al.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import bookr.al.dto.LoggedInUserInfoDto;
import bookr.al.dto.LoginRequestDto;
import bookr.al.dto.utils.LoggedInUserInfoUtils;
import bookr.bl.entity.IOrderableItem;
import bookr.bl.entity.OrderItem;
import bookr.bl.entity.UserAccounts;
import bookr.bl.entity.Users;
import bookr.bl.service.api.IOrderService;
import bookr.bl.service.api.IUserService;
import bookr.bl.service.api.IUserSessionService;
import bookr.bl.service.api.excp.AlreadyLoggedInException;
import bookr.bl.service.api.param.LoginRequest;
import bookr.common.utils.RestAssert;

/**
 * {@link RestController} for handling action on user session.
 * 
 * @author cherdsak.r
 *
 */
@RestController
public class UserSessionController {

	private static final Logger logger = LoggerFactory.getLogger(UserSessionController.class);
	
	@Autowired
	private IUserSessionService userSessionService;
	
	@Autowired 
	private HttpServletRequest request;
	
	/**
	 * Authenticate client based on given {@link LoginRequestDto} and establish user session.
	 * 
	 * @param loginRequestDto the detail of the login request
	 */
	@PostMapping("login")
	public void login(@RequestBody LoginRequestDto loginRequestDto) {
		String username = loginRequestDto.getUsername();
		String password = loginRequestDto.getPassword();

		RestAssert.hasLength(username, "username is required");
		RestAssert.hasLength(password, "password is required");
				
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsername(username);
		loginRequest.setPassword(password);
		
		try {
			this.userSessionService.login(loginRequest);			
		} catch (AlreadyLoggedInException e) {
			throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
		}

		// used by spring security Filters
		SecurityContext securityContext = SecurityContextHolder.getContext();		
		HttpSession session = request.getSession(true);
		session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, securityContext);
		
		logger.info("username: {} logged in", username);
	}

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IOrderService orderService;
	
	/**
	 * Gets information about the logged-in user.
	 * 
	 * @return
	 */
	@GetMapping("users")
	public LoggedInUserInfoDto getLoggedInUserInfo() {
		LoggedInUserInfoDto result = new LoggedInUserInfoDto();

		UserAccounts userAccount = this.userSessionService.getLoggedInUserAccount();
		
		String username = userAccount.getUsername();
		
		// load/populate respective user
		Users user = this.userService.findUserByUsername(username);		
		LoggedInUserInfoUtils.populate(result, user);
		
		// load/populate order
		List<IOrderableItem> items = this.orderService.findDistinctOrderedProductByKind(username, OrderItem.PRODUCTKIND_BOOK);
		List<Long> orderedBooks = items
					.stream()
						.map( item -> item.getId())
							.collect(Collectors.toList());

		result.setBooks(orderedBooks);
		
		return result;
	}

	/**
	 * Logout and remove user session.
	 * 
	 */
	@DeleteMapping("users")
	public void logout() {
		this.userSessionService.logout();
		HttpSession session = request.getSession();
		if (session != null) {
			session.invalidate();
		}	
	}
	
}
