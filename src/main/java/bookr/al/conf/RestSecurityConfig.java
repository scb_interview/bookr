package bookr.al.conf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import bookr.bl.service.api.IUserAccountService;
import bookr.bl.service.impl.BookrUserDetailsService;
import bookr.common.env.Profiles;

@EnableWebSecurity
public class RestSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private Environment environment;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
			
		// bookr
		http
			.httpBasic()
				.disable()
			.formLogin()
				.disable()
			.csrf()
				.disable()
		    .cors()
				.disable()
			.headers()
				.frameOptions()
				.disable();

		http.authorizeRequests()
				.antMatchers(HttpMethod.POST,"/login").permitAll()
				.antMatchers(HttpMethod.GET,"/books").permitAll()
				.antMatchers(HttpMethod.POST,"/users").permitAll();
		
		// h2-console
		boolean dbconsoleEnabled = this.environment.acceptsProfiles(Profiles.FEATURE_DBCONSOLE); 
		
		if (dbconsoleEnabled) {
			http
				.authorizeRequests()
					.antMatchers("/h2-console/*").permitAll();
		}

		http.authorizeRequests()
			.anyRequest()
			.authenticated();		
		
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		// swagger
		web
			.ignoring()
			.antMatchers("/v2/api-docs", "/swagger-resources", "/swagger-resources/**", "/configuration/security");
		// swagger ui
		boolean enableSwaggerUi = this.environment.acceptsProfiles(Profiles.FEATURE_SWAGGERUI);
		if (enableSwaggerUi) {
			web
			.ignoring()
			.antMatchers("/configuration/ui", "/configuration/ui", "/swagger-ui.html", "/webjars/**");			
		}
	}
	
	@Autowired
	private IUserAccountService userAccountService;
	
	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}	
	
	@Bean
	public DaoAuthenticationProvider authProvider() {
		BookrUserDetailsService userDetailsService = new BookrUserDetailsService();
		userDetailsService.setUserAccountService(this.userAccountService);

		DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
	    daoAuthenticationProvider.setUserDetailsService(userDetailsService);
	    daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
	    return daoAuthenticationProvider;
	}	
	
	@Bean(name = BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	 
}
