package bookr.al.conf;

import java.text.SimpleDateFormat;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;

@Configuration
public class JsonConfig {

	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder() {
	    Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		
	    // json pretty format
	    builder.featuresToEnable(SerializationFeature.INDENT_OUTPUT);
	    
		// use ISO 8601 date format
//		builder.dateFormat(new StdDateFormat());
	    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    builder.dateFormat(simpleDateFormat);
	    
	    builder.propertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
	    
	    return builder;
	}
	
}
