# Bookr RESTtful API #

## Solutions documentation
1. https://bitbucket.org/scb_interview/bookr/wiki/Home

---

## Developer get started guide

### Prerequisite
1. Know how to use git ( via CLI or GUI )
2. Know how to use Spring boot
3. Know how to use IDE ( Eclipse or InteliJ)
4. Know how to use Postman
    https://www.getpostman.com/

### Key takeaway
1. This application support multiple running profile
    It is by default active profile are env_dev, ft_dbconsole, ft_swaggerui
2. This application use H2 embedded database to support zero install which is best for demo but to support prod environment full database can be used. The application is database agnostic use no vender specific sql
3. Development tool support are turning on by default
    [h2 web database console](https://localhost:8080/h2-console/)
    [swagger ui](https://localhost:8080/swagger-ui.html)


### Steps
1. Clone to local repository
2. Build & run with IDE
3. See the api design
    Use browser to open https://localhost:8080/swagger-ui.html
4. See the tables
    Use browser to open https://localhost:8080/h2-console/
3. How to test api
    Use postman. Import src/test/postman/scb-bookr.postman_collection.json
    Select request from collection and Send
    This application use self-sign certificat. So you might need to turn of certificate validation  http://blog.getpostman.com/2014/01/28/using-self-signed-certificates-with-postman/
4. How to observe result
    See the data in tables https://localhost:8080/h2-console/
    See the http status/response from postman

---



